from .rfid import RFID
from .api import API
from .crypto import CryptoBox
from .display import Display


class Application:
    def __init__(self, url, device_key, server_key):
        self._api = API(url)
        self._crypto = CryptoBox(device_key, server_key, public_key=self._api.fetch_server_key)
        self._api.add_crypto(self._crypto)
        self._rfid = RFID()
        self._display = Display()

    def run(self):
        while True:
            try:
                self.add_device()
                self.wait_for_card()
            except KeyboardInterrupt:
                return 0
            except Exception:
                pass

    def wait_for_card(self):
        while True:
            card_id = self._rfid.read_card()
            self._display.clear()
            status = self._api.create_event(card_id)
            if status:
                self._display.show_status(status)

    def add_device(self):
        while not self._api.is_device_trusted():
            self._display.show_key('S', self._crypto.server_key)
            self._rfid.read_card()
            self._display.show_key('D', self._crypto.device_key)
            self._rfid.read_card()
        self._crypto.save_keys()
        self._display.clear()
