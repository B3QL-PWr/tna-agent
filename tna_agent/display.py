import os
from threading import Timer
from PIL import ImageFont
from luma.core.render import canvas
from luma.oled.device import ssd1306


class Display:
    def __init__(self):
        base_dir = os.path.dirname(os.path.abspath(__file__))
        self._font = ImageFont.truetype(os.path.join(base_dir, 'fonts/FreeSans.ttf'))
        self._device = ssd1306(height=32)
        self._timer = None

    def show_status(self, status, time=0.5):
        self._set_clear_timer(time)
        font = self._font.font_variant(size=24)
        with canvas(self._device) as draw:
            draw.text(self.center_text(status, font), text=status, font=font, fill='white')

    def center_text(self, text, font):
        device_width, device_height = self._device.size
        font_width, font_height = font.getsize(text)
        width_diff = device_width - font_width
        height_diff = device_height - font_height
        return (width_diff / 2, height_diff / 2)

    def show_key(self, type_letter, key):
        self._stop_clear_timer()
        font = self._font.font_variant(size=10)
        text = list(self._chunks(key, 22))
        text[-1] += '  ' + type_letter
        with canvas(self._device) as draw:
            for lineno, line in enumerate(text):
                line_start = lineno * font.getsize(line)[1]
                draw.text((0, line_start), line, font=font, fill='white')

    def _chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i + n]

    def clear(self):
        return self._device.clear()

    def _stop_clear_timer(self):
        if self._timer:
            self._timer.cancel()

    def _set_clear_timer(self, interval):
        self._stop_clear_timer()
        self._timer = Timer(interval, self.clear)
        self._timer.start()
