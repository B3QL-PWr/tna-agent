from datetime import datetime
import pirc522


class RFID:
    def __init__(self):
        self._rfid = pirc522.RFID()
        self._last_card_id = None
        self._last_card_time = 0

    def read_card(self, interval=1):
        while True:
            self._rfid.wait_for_tag()
            error, _ = self._rfid.request()
        
            if error:
                continue

            error, uid = self._rfid.anticoll()
            if not error:
                card_id = self._format(uid)
                now = datetime.now().timestamp()
                if card_id != self._last_card_id or now - self._last_card_time >= interval:
                    self._last_card_id = card_id
                    self._last_card_time = now
                    return card_id

    def _format(self, uid):
        return ''.join('{:02X}'.format(part) for part in uid)

    def __del__(self):
        self._rfid.cleanup()
