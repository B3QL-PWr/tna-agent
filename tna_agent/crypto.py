from nacl.public import Box, PublicKey, PrivateKey
from nacl.encoding import HexEncoder


class ExtractEncoder:
    @staticmethod
    def decode(data):
        return data._private_key


class CryptoBox:
    def __init__(self, private_key_file, public_key_file=None, public_key=None):
        self._private_key_file = private_key_file
        self._private_key = self._load_or_generate(private_key_file, PrivateKey, PrivateKey.generate, encoder=ExtractEncoder)
        self._public_key = self._load_or_generate(public_key_file, PublicKey, public_key, encoder=HexEncoder)
        self._public_key_file = public_key_file
        self._box = Box(self._private_key, self._public_key)

    def _load_or_generate(self, filename, cls, fallback_key, *args, **kwargs):
        try:
            return self._load_key(filename, cls)
        except FileNotFoundError:
            if callable(fallback_key):
                key = fallback_key()
            else:
                key = fallback_key
        return cls(key, *args, **kwargs)

    def _save_key(self, filename, key, force=False):
        mode = 'w+' if force else 'x+'
        try:
            with open(filename, mode) as f:
                return f.write(key.encode(HexEncoder).decode())
        except FileExistsError:
            return 0

    def _load_key(self, filename, cls):
        with open(filename) as f:
            return cls(f.read(), encoder=HexEncoder)

    def encrypt(self, data):
        try:
            return self._box.encrypt(str(data).encode(), encoder=HexEncoder).decode()
        except ValueError:
            return None

    def decrypt(self, data):
        try:
            return self._box.decrypt(str(data).encode(), encoder=HexEncoder).decode()
        except ValueError:
            return None

    @property
    def device_key(self):
        return self._private_key.public_key.encode(HexEncoder).decode()

    @property
    def server_key(self):
        return self._public_key.encode(HexEncoder).decode()

    def save_keys(self):
        self._save_key(self._private_key_file, self._private_key)
        self._save_key(self._public_key_file, self._public_key)
