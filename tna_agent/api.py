from urllib.parse import urljoin
import requests


class CryptoBoxMissing(Exception):
    pass


def crypto_required(func):
    def wrapper(*args, **kwargs):
        obj = args[0]
        if obj._box:
            return func(*args, **kwargs)
        raise CryptoBoxMissing('Calling {} requires crypto'.format(func.__name__))
    return wrapper


class API:
    PUBLIC_KEY_ENDPOINT = '/api/public_key'
    EVENTS_ENDPOINT = '/api/events'
    DEVICES_ENDPOINT = '/api/devices'

    def __init__(self, url, crypto=None):
        self._base_url = url
        self._box = crypto

    def _endpoint_url(self, endpoint, *segments):
        url_segments = [endpoint]
        url_segments.extend(segments)
        url_path = '/'.join(url_segments)
        return urljoin(self._base_url, url_path)

    def fetch_server_key(self):
        url = self._endpoint_url(self.PUBLIC_KEY_ENDPOINT)
        r = requests.get(url)
        r.raise_for_status()
        return r.text

    @crypto_required
    def is_device_trusted(self):
        url = self._endpoint_url(self.DEVICES_ENDPOINT, self._box.device_key)
        r = requests.get(url)
        if r.ok:
            return self._box.decrypt(r.text) is not None
        return False

    def add_crypto(self, crypto):
        self._box = crypto

    @crypto_required
    def create_event(self, card_id):
        url = self._endpoint_url(self.EVENTS_ENDPOINT, self._box.device_key)
        r = requests.put(url, data=self._box.encrypt(card_id))
        r.raise_for_status()
        return self._box.decrypt(r.text)