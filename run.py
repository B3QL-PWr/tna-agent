#!/usr/bin/env python3
from tna_agent.app import Application

if __name__ == '__main__':
    app = Application(url='http://10.42.0.1:8000', device_key='device.key', server_key='server.pub')
    app.run()
